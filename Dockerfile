FROM golang:1.14.13-alpine3.12 AS build

RUN apk add gcc musl-dev make

ENV APP_HOME=/go/src/app/

WORKDIR $APP_HOME

COPY go.* $APP_HOME

RUN go mod download

ADD . $APP_HOME

RUN make build


FROM alpine:3.12.1 AS production

ENV APP_HOME=/opt/

WORKDIR $APP_HOME

COPY --from=build /go/src/app/fxproxy_v* $APP_HOME

CMD ./fxproxy_v*
