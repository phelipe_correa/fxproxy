package main

import (
	"testing"

	"net/http"
  "net/http/httptest"
	"os"
	"github.com/gorilla/mux"

	"github.com/stretchr/testify/require"
)

func dummyAllowHandler(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte("dummyResponse"))
}

func dummyDenyHandler(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(http.StatusForbidden)
}

func TestDrawRoutes(t *testing.T) {
	var cases = []struct {
		path     string
		httpCode int
	}{
		{"/company", http.StatusOK},
		{"/company/sd45f768", http.StatusOK},
		{"/company/account", http.StatusOK},
		{"/acc734340", http.StatusOK},
		{"/account/acc74850", http.StatusOK},
		{"/account/acc234234/user", http.StatusOK},
		{"/account/blocked", http.StatusForbidden},
		{"/tenant/sj3co3s4", http.StatusNotFound},
		{"/tenant/account/blocked", http.StatusOK},
		{"/tenant/account/acc23849", http.StatusNotFound},
	}

	be := httptest.NewServer(http.HandlerFunc(dummyAllowHandler))
	defer be.Close()
	os.Setenv("DOWNSTREAM", be.URL)

	r := mux.NewRouter()

	drawRoutes(r)

	for _, c := range cases {
		rr := httptest.NewRecorder()

		r.ServeHTTP(rr, httptest.NewRequest("GET", c.path, nil))

		require.Equal(t, c.httpCode, rr.Code, "Test is failing!")
	}
}

func TestBuildRoutes(t *testing.T) {
	var cases = []struct {
		httpCode int
		path     string
	}{
		{http.StatusOK, "/dummy"},
		{http.StatusOK, "/dummy2"},
		{http.StatusNotFound , "/unknown"},
	}

	router := mux.NewRouter()
	testRoutes := []string{"/dummy", "/dummy2"}
	buildRoutes(router, testRoutes, dummyAllowHandler)

	for _, c := range cases {
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, httptest.NewRequest("GET", c.path, nil))

		require.Equal(t, c.httpCode, rr.Code, "Test is failing!")
	}
}

func TestAllowRoutes(t *testing.T) {
	var defaultRoutes = []string{
		"/company/",
		"/company/{id}",
		"/company/account",
		"/account",
		"/account/{id}",
		"/{id}",
		"/account/{id}/user",
		"/tenant/account/blocked",
	}

	require.Equal(t, defaultRoutes, allowRoutes(), "Test is failing!")
}

func TestDenyHandler(t *testing.T) {
	var cases = []struct {
		httpCode     int
		path         string
	}{
		{http.StatusForbidden, "/forbidden-path"},
	}

	h := http.HandlerFunc(denyHandler)

	for _, c := range cases {
		rr := httptest.NewRecorder()

		h.ServeHTTP(rr, httptest.NewRequest("GET", c.path, nil))

		require.Equal(t, c.httpCode, rr.Code, "Test is failing!")
	}
}

func TestProxyHandler(t *testing.T) {
	var cases = []struct {
		httpCode     int
		responseBody string
		path         string
	}{
		{http.StatusOK, "dummyResponse", "/"},
	}

	be := httptest.NewServer(http.HandlerFunc(dummyAllowHandler))
	defer be.Close()

	os.Setenv("DOWNSTREAM", be.URL)
	h := http.HandlerFunc(proxyHandler)

	for _, c := range cases {
		rr := httptest.NewRecorder()

		h.ServeHTTP(rr, httptest.NewRequest("GET", c.path, nil))

		require.Equal(t, c.httpCode, rr.Code, "Test is failing!")
		require.Equal(t, c.responseBody, rr.Body.String(), "Test is failing!")
	}
}
