require 'sinatra'

get '/company' do
  'The best company in the whole world!'
end

get '/company/:id' do
  "The company ##{params[:id]}!"
end

get '/company/account' do
  "The company's account!"
end

get '/account' do
  "Your account!"
end

get '/account/:id' do
  "Account's ##{params[:id]}!"
end

get '/:id' do
  "ID ##{params[:id]}!"
end

get '/account/:id/user' do
  "User from Account ##{params[:id]}!"
end

get '/tenant/account/blocked' do
  "Tenant from account blocked!"
end
