VERSION     = 1.0.0
BINARY      = fxproxy_v${VERSION}_${OS_ARCH}
GOARCH      = amd64
CURRENT_DIR = $(shell pwd)
OS          = $(shell uname | awk '{print tolower($$0)}')
OS_ARCH     = ${OS}_x64

all: build run

build:
	cd ${CURRENT_DIR}; \
	GOOS=${OS} GOARCH=${GOARCH} CGO_ENABLED=1 go build -v -o ${BINARY}

run:
	./fxproxy_v*
