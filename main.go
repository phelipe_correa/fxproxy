package main

import (
	"net/http/httputil"
	"net/http"
	"net/url"
	"log"
	"time"
	"os"
	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()

	drawRoutes(r)

	s := server(r)
	defer s.Close()

	log.Println("Starting server on", os.Getenv("UPSTREAM"))
	log.Fatal(s.ListenAndServe())
}

func server(h http.Handler) *http.Server {
	return &http.Server{
		Handler: h,
		Addr: os.Getenv("UPSTREAM"),
		WriteTimeout: 60 * time.Second,
		ReadTimeout: 60 * time.Second,
	}
}

func drawRoutes(r *mux.Router) {
	buildRoutes(r, denyRoutes(), denyHandler)
	buildRoutes(r, allowRoutes(), proxyHandler)
}

func buildRoutes(r *mux.Router, routes []string, f func(http.ResponseWriter, *http.Request)) {
	for _,route := range routes {
    r.HandleFunc(route, f)
	}
}

func denyRoutes () []string {
	return []string{
		"/account/blocked",
	}
}

func allowRoutes () []string {
	return []string{
		"/company/",
		"/company/{id}",
		"/company/account",
		"/account",
		"/account/{id}",
		"/{id}",
		"/account/{id}/user",
		"/tenant/account/blocked",
	}
}

func denyHandler(rw http.ResponseWriter, re *http.Request) {
	rw.WriteHeader(http.StatusForbidden)
}

func proxyHandler(rw http.ResponseWriter, re *http.Request) {
	url, err := url.Parse(os.Getenv("DOWNSTREAM"))
	if err != nil {
		log.Fatal("Error parsing url")
	}

	proxy := httputil.NewSingleHostReverseProxy(url)
	proxy.ServeHTTP(rw, re)
}
